import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:my_chat_demo/model/user_chat.dart';

import '../constant/firestore_constants.dart';
import '../model/messgae_model.dart';

class HomeProvider with ChangeNotifier {
  List<UserChat> contacts = [];

  Future fetchContact() async {
    contacts = [];
    final QuerySnapshot result = await FirebaseFirestore.instance
        .collection(FireStoreConstants.pathUserCollection)
        .where(FireStoreConstants.id,
            isNotEqualTo: FirebaseAuth.instance.currentUser!.uid)
        .get();

    for (var e in result.docs) {
      contacts.add(UserChat.fromDocument(e));
    }

    notifyListeners();
  }

  List<MessageModel> messages = [];

  Future fetchMessages() async {
    FirebaseFirestore.instance
        .collection(FireStoreConstants.pathThreadCollection)
        .doc(threadId())
        .collection(FireStoreConstants.pathMessagesCollection)
        .orderBy('time')
        .snapshots()
        .listen((event) {
      messages = [];
      for (var e in event.docs) {
        messages.add(MessageModel.fromJson(e));
      }
      notifyListeners();
    });
  }

  int? indexUserSelected;

  Future sendMessage(String content) {
    MessageModel messageModel = MessageModel(
      content,
      FirebaseAuth.instance.currentUser!.uid,
      contacts[indexUserSelected ?? 0].id,
      DateTime.now(),
    );
    return FirebaseFirestore.instance
        .collection(FireStoreConstants.pathThreadCollection)
        .doc(threadId())
        .collection(FireStoreConstants.pathMessagesCollection)
        .add(messageModel.toJson());
  }

  String threadId() {
    if (FirebaseAuth.instance.currentUser!.uid
            .compareTo(contacts[indexUserSelected ?? 0].id) >
        0) {
      return '${FirebaseAuth.instance.currentUser!.uid}_${contacts[indexUserSelected ?? 0].id}';
    } else {
      return '${contacts[indexUserSelected ?? 0].id}_${FirebaseAuth.instance.currentUser!.uid}';
    }
  }
}
