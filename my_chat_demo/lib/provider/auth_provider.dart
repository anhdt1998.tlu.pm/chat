import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:my_chat_demo/constant/firestore_constants.dart';
import 'package:my_chat_demo/model/user_chat.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum Status {
  uninitialized,
  authenticated,
  authenticating,
  authenticateError,
  authenticateCanceled,
}

class AuthProvider with ChangeNotifier {
  final GoogleSignIn googleSignIn;
  final SharedPreferences prefs;

  Status _status = Status.uninitialized;

  Status get status => _status;

  AuthProvider({
    required this.googleSignIn,
    required this.prefs,
  });

  String? getUserFirebaseId() {
    return prefs.getString(FireStoreConstants.id);
  }

  Future<bool> isLoggedIn() async {
    bool isLoggedIn = await googleSignIn.isSignedIn();
    if (isLoggedIn &&
        prefs.getString(FireStoreConstants.id)?.isNotEmpty == true) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> handleSignIn() async {
    _status = Status.authenticating;
    notifyListeners();

    GoogleSignInAccount? googleUser = await googleSignIn.signIn();
    if (googleUser != null) {
      GoogleSignInAuthentication? googleAuth = await googleUser.authentication;
      final AuthCredential credential = GoogleAuthProvider.credential(
          accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);

      User? firebaseUser =
          (await FirebaseAuth.instance.signInWithCredential(credential)).user;

      if (firebaseUser != null) {
        final QuerySnapshot result = await FirebaseFirestore.instance
            .collection(FireStoreConstants.pathUserCollection)
            .where(FireStoreConstants.id, isEqualTo: firebaseUser.uid)
            .get();

        final List<DocumentSnapshot> documents = result.docs;
        if (documents.isEmpty) {
          FirebaseFirestore.instance
              .collection(FireStoreConstants.pathUserCollection)
              .doc(firebaseUser.uid)
              .set({
            FireStoreConstants.nickname: firebaseUser.displayName,
            FireStoreConstants.photoUrl: firebaseUser.photoURL,
            FireStoreConstants.id: firebaseUser.uid,
            'createdAt': DateTime.now().microsecondsSinceEpoch.toString(),
          });

          User? currentUser = firebaseUser;
          await prefs.setString(FireStoreConstants.id, currentUser.uid);
          await prefs.setString(
              FireStoreConstants.nickname, currentUser.displayName ?? '');
          await prefs.setString(
              FireStoreConstants.photoUrl, currentUser.photoURL ?? '');
        } else {
          DocumentSnapshot documentSnapshot = documents[0];
          UserChat userChat = UserChat.fromDocument(documentSnapshot);

          await prefs.setString(FireStoreConstants.id, userChat.id);
          await prefs.setString(
              FireStoreConstants.nickname, userChat.nickName ?? '');
          await prefs.setString(
              FireStoreConstants.photoUrl, userChat.photoUrl ?? '');
        }

        _status = Status.authenticated;
        notifyListeners();
        return true;
      } else {
        _status = Status.authenticateError;
        notifyListeners();
        return false;
      }
    } else {
      _status = Status.authenticateCanceled;
      notifyListeners();
      return false;
    }
  }

  Future<void> handleSignOut() async {
    _status = Status.uninitialized;
    await FirebaseAuth.instance.signOut();
    await googleSignIn.disconnect();
    await googleSignIn.signOut();
  }
}
