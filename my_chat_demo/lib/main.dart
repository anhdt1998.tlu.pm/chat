import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:my_chat_demo/provider/auth_provider.dart';
import 'package:my_chat_demo/provider/home_provider.dart';
import 'package:my_chat_demo/screen/splash_page.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SharedPreferences prefs = await SharedPreferences.getInstance();

  runApp(MyApp(prefs: prefs));
}

class MyApp extends StatelessWidget {
  final SharedPreferences prefs;

  const MyApp({Key? key, required this.prefs}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => AuthProvider(
            googleSignIn: GoogleSignIn(),
            prefs: prefs,
          ),
        ),
        ChangeNotifierProvider(create: (_) => HomeProvider())
      ],
      child: MaterialApp(
        title: 'My Chat App',
        theme: ThemeData(
          primaryColor: Colors.black,
        ),
        home: const SplashPage(),
      ),
    );
  }
}
