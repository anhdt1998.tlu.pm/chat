class FireStoreConstants {
  static const pathUserCollection = "users";
  static const pathThreadCollection = "thread";
  static const pathMessagesCollection = "message";

  static const nickname = "nickname";
  static const aboutMe = "aboutMe";
  static const photoUrl = "photoUrl";
  static const phoneNumber = "phoneNumber";
  static const id = "id";
}
