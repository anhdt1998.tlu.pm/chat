import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:my_chat_demo/constant/firestore_constants.dart';

class UserChat {
  String id;
  String photoUrl;
  String nickName;

  UserChat({
    required this.id,
    required this.photoUrl,
    required this.nickName,
  });

  Map<String, String> toJson() {
    return {
      FireStoreConstants.id: id,
      FireStoreConstants.nickname: nickName,
      FireStoreConstants.photoUrl: photoUrl,
    };
  }

  factory UserChat.fromDocument(DocumentSnapshot doc) {
    String photoUrl = '';
    String nickName = '';
    try {
      photoUrl = doc.get(FireStoreConstants.photoUrl);
      nickName = doc.get(FireStoreConstants.nickname);
    } catch (e) {
      rethrow;
    }

    return UserChat(
      id: doc.id,
      photoUrl: photoUrl,
      nickName: nickName,
    );
  }
}
