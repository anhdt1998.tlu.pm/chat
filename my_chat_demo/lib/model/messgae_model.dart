import 'package:cloud_firestore/cloud_firestore.dart';

class MessageModel {
  String? id;

  String? content;

  String? from;

  String? to;

  DateTime? time;

  MessageModel(this.content, this.from, this.to, this.time);

  MessageModel.fromJson(dynamic json) {
    id = json['id'];
    content = json['content'];
    from = json['from'];
    to = json['to'];
    time = (json['time'] as Timestamp).toDate();
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['content'] = content;
    map['from'] = from;
    map['to'] = to;
    map['time'] = Timestamp.fromDate(time!);
    return map;
  }
}
