import 'package:flutter/material.dart';
import 'package:my_chat_demo/constant/color_constants.dart';
import 'package:my_chat_demo/model/user_chat.dart';
import 'package:my_chat_demo/provider/home_provider.dart';
import 'package:my_chat_demo/widget/loading_view.dart';
import 'package:provider/provider.dart';

import '../widget/message_widget.dart';

class ChatDetailPage extends StatefulWidget {
  final UserChat user;

  const ChatDetailPage({
    Key? key,
    required this.user,
  }) : super(key: key);

  @override
  State<ChatDetailPage> createState() => _ChatDetailPageState();
}

class _ChatDetailPageState extends State<ChatDetailPage> {
  late HomeProvider chatDetailProvider;
  late TextEditingController textEditingController;

  @override
  void initState() {
    super.initState();
    chatDetailProvider = context.read<HomeProvider>();
    textEditingController = TextEditingController();
    chatDetailProvider.fetchMessages();
  }

  @override
  Widget build(BuildContext context) {
    final messages = context.watch<HomeProvider>().messages;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorConstants.themeColor.withOpacity(0.6),
        title: Text(widget.user.nickName),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemBuilder: (context, index) {
                return MessageWidget(
                  message: messages[index],
                  friend: chatDetailProvider
                      .contacts[chatDetailProvider.indexUserSelected ?? 0],
                );
              },
              itemCount: messages.length,
            ),
          ),
          Container(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
              decoration: BoxDecoration(
                  border: Border(top: Divider.createBorderSide(context))),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      controller: textEditingController,
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(width: 1, color: Colors.white),
                            borderRadius: BorderRadius.circular(60.0),
                          ),
                          hintText: 'Send message...',
                          hintStyle: const TextStyle(color: Colors.white),
                          filled: true,
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(60.0),
                            borderSide:
                                const BorderSide(width: 1, color: Colors.white),
                          ),
                          fillColor:
                              ColorConstants.themeColor.withOpacity(0.5)),
                    ),
                  ),
                  const SizedBox(
                    width: 12,
                  ),
                  IconButton(
                    onPressed: () {
                      if (textEditingController.text.trim().isNotEmpty) {
                        String content = textEditingController.text.trim();
                        textEditingController.clear();
                        chatDetailProvider
                            .sendMessage(content)
                            .then((value) {});
                      }
                    },
                    icon: Icon(
                      Icons.send,
                      color: ColorConstants.themeColor.withOpacity(0.7),
                    ),
                  )
                ],
              ))
        ],
      ),
    );
  }
}
