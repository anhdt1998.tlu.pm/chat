import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:my_chat_demo/provider/auth_provider.dart';
import 'package:my_chat_demo/screen/home_page.dart';
import 'package:my_chat_demo/widget/loading_view.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  static const routeName = 'LoginPage';

  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  static const tag = 'LoginPage';
  late AuthProvider authProvider;

  @override
  void initState() {
    super.initState();
    authProvider = context.read<AuthProvider>();
  }

  @override
  Widget build(BuildContext context) {
    switch (authProvider.status) {
      case Status.authenticateError:
        Fluttertoast.showToast(msg: 'SignIn Fail!');
        break;
      case Status.authenticateCanceled:
        Fluttertoast.showToast(msg: 'SignIn Canceled!');
        break;
      case Status.authenticated:
        Fluttertoast.showToast(msg: 'SignIn Success!');
        break;
      default:
        break;
    }

    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Image.asset('assets/back.png'),
          ),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: GestureDetector(
              onTap: () async {
                bool isSuccess = await authProvider.handleSignIn();
                if (isSuccess) {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const HomePage()));
                }
              },
              child: Image.asset('assets/google_login.jpg'),
            ),
          ),
          SizedBox(
              child: authProvider.status == Status.authenticating
                  ? const LoadingView()
                  : const SizedBox.shrink())
        ],
      ),
    );
  }
}
