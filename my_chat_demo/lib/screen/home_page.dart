import 'package:flutter/material.dart';
import 'package:my_chat_demo/constant/color_constants.dart';
import 'package:my_chat_demo/model/user_chat.dart';
import 'package:my_chat_demo/provider/auth_provider.dart';
import 'package:my_chat_demo/provider/home_provider.dart';
import 'package:my_chat_demo/screen/chat_detail_page.dart';
import 'package:my_chat_demo/screen/login_page.dart';
import 'package:my_chat_demo/widget/loading_view.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  static const routeName = 'HomePage';

  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  static const tag = 'HomePage';
  late AuthProvider authProvider;
  late HomeProvider homeProvider;
  late String currentId;

  @override
  void initState() {
    super.initState();
    authProvider = context.read<AuthProvider>();
    homeProvider = context.read<HomeProvider>();
    if (authProvider.getUserFirebaseId()?.isNotEmpty == true) {
      currentId = authProvider.getUserFirebaseId()!;
    } else {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => const LoginPage()));
    }
    homeProvider.fetchContact();
  }

  @override
  Widget build(BuildContext context) {
    final contacts = context.watch<HomeProvider>().contacts;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorConstants.themeColor.withOpacity(0.6),
        title: const Text('Chat'),
        actions: [
          IconButton(
              onPressed: () {
                authProvider.handleSignOut().then((value) =>
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const LoginPage())));
              },
              icon: const Icon(Icons.exit_to_app))
        ],
      ),
      body: contacts.isEmpty
          ? const Center(
              child: Text('No Contact'),
            )
          : Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListView.separated(
                  itemBuilder: (context, index) {
                    return ItemChatUser(
                      userChat: contacts[index],
                      onTap: () {
                        homeProvider.indexUserSelected = index;
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    ChatDetailPage(user: contacts[index])));
                      },
                    );
                  },
                  separatorBuilder: (context, index) => const Divider(),
                  itemCount: contacts.length),
            ),
    );
  }
}

class ItemChatUser extends StatelessWidget {
  final UserChat userChat;
  final VoidCallback onTap;

  const ItemChatUser({Key? key, required this.userChat, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkResponse(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  image:
                      DecorationImage(image: NetworkImage(userChat.photoUrl))),
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  userChat.nickName,
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  userChat.id,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
