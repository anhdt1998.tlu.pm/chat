import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:my_chat_demo/constant/color_constants.dart';
import 'package:my_chat_demo/model/user_chat.dart';

import '../model/messgae_model.dart';

class MessageWidget extends StatelessWidget {
  final UserChat friend;
  final MessageModel message;

  const MessageWidget({
    Key? key,
    required this.message,
    required this.friend,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isFromMe = FirebaseAuth.instance.currentUser!.uid == message.from;

    return Container(
      padding: const EdgeInsets.all(8).copyWith(
        right: isFromMe ? 8 : 48,
        left: isFromMe ? 48 : 8,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (!isFromMe) ...[
            Container(
              width: 30,
              height: 30,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  image: DecorationImage(image: NetworkImage(friend.photoUrl))),
            ),
            const SizedBox(
              width: 10,
            )
          ],
          Expanded(
            child: Align(
              alignment:
                  isFromMe ? Alignment.centerRight : Alignment.centerLeft,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: isFromMe
                      ? const BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20),
                          bottomLeft: Radius.circular(20),
                        )
                      : const BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20),
                          bottomRight: Radius.circular(20),
                        ),
                  color: isFromMe
                      ? ColorConstants.themeColor.withOpacity(0.6)
                      : Colors.grey[200],
                ),
                padding:
                    const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: isFromMe
                      ? CrossAxisAlignment.end
                      : CrossAxisAlignment.start,
                  children: [
                    Text(
                      message.content ?? '',
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: isFromMe ? Colors.white : Colors.black),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Text(
                      DateFormat('HH:mm').format(message.time!),
                      style: Theme.of(context).textTheme.caption!.copyWith(
                          color: isFromMe ? Colors.white : Colors.black),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
